Cos'è
=====
è un telegram-bot

Cosa fa (o dovrebbe fare)
=========================
Quando si è in giro si possono mandare file di diverso genere (coordinate, foto, audio, video, pdf e qualsiasi altro file)   
e questi file vengono gestiti, solitemente mettendoli online.

Cosa succede se si inviano messagi, file, comandi
=================================================
Se si inviano messaggi che non siano comandi o file, allora il testo del messaggio viene memorizzato.

Nel caso il messaggio contenga un comando, allora viene eseguito il comando

Nel caso il messaggio sia l'invio di un file, questo file viene memoriazzato


Messaggi normali (non implementato)
================
Questi messaggi vengono memorizzati (nella directory del **giro**) in un file che tiene conto del momento dell'invio del messaggio e di chi lo ha spedito.
(idea: YYYYMMDD-HHMMSS-Utente.txt). Cosa succeda con questo file non si sa. Si potrebbe fare un broadcast a tutti coloro che fanno parte del **giro**
(si rischia di trasformare il bot in una sorta di chat di gruppo, con tanto di vantaggi e svantaggi).

L'idea però sarebbe quella di aggiungere in automatico questi testi ad una sorta di "diario" (che conterrà anche foto e itinerari)

I comandi
=========
/decode
-------------
Il comando  ```/decode``` resistituisce semplicemente l'oggetto JSON che viene generato con il comando stesso. 
Dal JSON è possibile per esempio conoscere l'identificativo di chi scrive (utile per quando si voglio dare/togliere diritti agli utente).

/new
-------------
Il comando  ```/new``` è riservato all'amministratore dei giri. Permette di creare un nuovo "giro" e lo rende anche attivo.

/select
-------------
```/select``` è riservato agli "amministratori di giri". Permette di impostare il nome del nuovo "giro attivo". Questo deve essere già esistente. 
Eventualemtne lo si può creare con il comando ```/new``` (che oltre a crearlo, lo seleziona). Con ```/active``` si può in qualsiasi momento chiedere quale sia il giro attivo.

/active
-------------
Con ```/active``` chiunque può chiedere quale sia attualmente il "giro" attivo (per crearne uno nuovo si usa ```/new```, per selezionarne uno già esistente si usa ```/select```)

/list
-------------
```/list``` elenca tutti i "giri" che già sono stati creati e dunque disponibili per essere selezionati (tramite ```/select```)


/help
------
```/help``` senza ulteriori parametri, elenca tutti i comandi possibili e l'aggiunta che scrivendo ```/help /comando``` si ottiene una spiegazione specifica per ```/comando```.

```/help /comando``` (dove  ```/comando``` è un comando possibile), da qualche spiegazione specifica per tale comando

Ancora da implementare: Finisce sempre indicando che con ```/getmanual``` si può ottenere un pdf con tutte le spiegazioni. Mettere eventualmente il link al pdf.

/getmanual (non implementato)
-----------------------
Con ```/getmanual``` si ottiene un pdf (e/o un link ad una pagina html) con tutte le spiegazioni ( e/o un link ad una pagina html)

/info (non implementato)
------------------------
```/info``` senza ulteriori parametri, racconta qualcosa sul **giro** attualmente selezionato.

```/info <nomegiro>``` racconta qualcosa sul **giro** ```nomegiro``` (se se ne ha il diritto)

/setinfo, /addinfo, /delinfo  (non implementato)
-----------------------
permette di dare, aggiungere, scuotare le informazioni al giro attivo (o indicato con il parametro)

/user, /admin (non implementato)
------------------------
Elenca gli utenti e admin e i loro diritti.

Cosa succede quando si inviano file
===================================
Foto
--------
Vengono salvate nella directory corrispondente al "giro". 
A seconda di come sono state inviate, vengono memorizzate versioni diverse sia per quanto riguarda la qualità che anche per quanto riguarda il nome del file.

### Facendo la foto da dentro Telegram
Se la foto viene inviata facendola da dentro Telegram, allora il file non contiene alcun EXIF (e dunque ne la data di creazione e neanche eventuali coordinate geografiche).
Inoltre la qualità (buona per applicazioni su computer, ma non per stampare su carta) dell'immagine la decide Telegram e non chi fa la foto.
Il nome del file è una lunga sequenza casuale di caratteri alfanumerici.

Bisognerebbe fare che un evenutale commento fatto insieme alla foto, venga abbinato alla foto.

Bisognerebbe fare che il nome del file abbia un senso (p.es. YYYYMMDD-HHMMSSmmmm-NomeUtente.jpg) e il commento vada in un file con lo stesso nome (salvo estensione che diventerebbe .txt)

### Allegando una foto già fatta, prelevandola dalla galleria

### Allegando una foto come se fosse un file qualsiasi


Coordinate geografiche
---------------
### Inviando un file gpx come se fosse un file qualsiasi
Vengono salvate nella directory corrispondente al "giro". 

### Inviando coordinate con la funzione interna di Telegram
Vengono salvate nel file ```latlon.tsv``` nella directory corrispondente al "giro". 
Viene memorizzato anche il "momento" e chi lo ha inviato (id, first_name, last_name, username).




I parametri (configurazione)
===========
Giro attivo
-----------
Si tratta di un modo di organizzare il materiale che arriva, posizionadolo in direcotry separate che portano esattamente quel nome. 

"giri" scritti uguali ma che si distinguono anche soltanto da una minuscola/maiuscola, risulteranno giri differenti (*case sensitive*).

Viene memorizzato nel file ```config/giroAttivo.txt```. Con il comando ```/active``` si viene sapere come si chiama il "giro attivo" attualmente in uso. 
Puuò essere modificato solo dagli "amministratori di giri". Con il comando  ```/select``` si sceglie quale "giro" (già esistente) deve diventare quello attivo.
Con il comando ```/new``` si crea un nuovo "giro" e lo si rende automaticamente quello attivo.

Utenti e amministratori
-----------------------


Ci sono utenti più uguali di altri
==================================
Noti e ignoti
-------------
Per colloquiare con il bot bisogna essere anzitutto un **utente autorizzato**.

Amministratore generale
-----------------------
Può tutto, anche dare e togliere i livelli di autorizzazione. Non può perdere tale livello.

Amministratore delegato generale (non implementato)
---------------------------
può creare nuovi giri e selezionarli. Può creare **amministori delegati speciali** per singoli progetti.

Amministratore delegato speciale (non implementato)
----------------------------------

Utente autorizzato generale
---------------------------
Può spedire file e dare comandi (non è detto che vengano presi in cosiderazione) , indipendentemente da quale **giro** sia attivo.

Utente autorizzato speciale (non implementato)
---------------------------------------
può dare comandi (non è detto che vengano presi in cosiderazione) per tutti i **giri**, ma spedire file  solo per determinati **giri**

Dietro le quinte
=========

TODO
====

* creare il comando ```/getmanual``` con il quale si può ottenere un pdf (o link a pagina html) con tutte le spiegazioni
* creare il comando ```/refresh``` che fa partire una qualche procedura specifica per il **giro**.
* creare il comando ```/addadmin```, ```/deladmin```, per aggiungere o cancellare un **amministratore delegato generale**
* creare il comando ```/adduser```,  ```/deluser```, per aggiungere un **utente autorizzato generale**
* crere in ```config``` uno o più file con l'elenco degli **utenti autorizzati** e con i loro livelli di autorizzazione
* dare all'**amministratore generale** la possibilità di aggiungere nuovi utenti autorizzati (o toglere loro i privilegi)
* creare diversi livelli di autorizzazione: 
  * **amministratore generale**: può tutto, anche dare e togliere i livelli di autorizzazione. Non può perdere tale livello.
  * **amministratore delegato generale**: può creare nuovi giri e selezionarli. Può creare **amministori delegati speciali** per singoli progetti.
  * **amministratore delegato speciale**: lo è solo per determinati "giri", per i quali può stabilire se un determinato **utente è autorizzato** o meno
  * **utente autorizzato generale**: può spedire file e dare comandi (non è detto che vengano presi in cosiderazione) per tutti i **giri**
  * **utente autorizzato speciale**: può dare comandi (non è detto che vengano presi in cosiderazione) per tutti i **giri**, ma spedire file  solo per determinati **giri**
* fare in modo che ```/help``` prelevi le informazioni da un file TXT e non siano hardcoded. 
* fare in modo che le parole riservate ai **comandi** e che non possono essere usate per nomi di **giri** vengano da una lista (p.es. sottodirectory di config)

