---
title: "gitlab-Help"
output: html_document
---
Command line instructions
=========================
Git global setup
----------------

    git config --global user.name "Antonio Gulino"
    git config --global user.email "mai@address.org"

Create a new repository
-----------------------


    git clone https://gitlab.com/antoniogulino/TelegramBot.git
    cd TelegramBot
    touch README.md
    git add README.md
    git commit -m "add README"
    git push -u origin master

Existing folder
-----------------------


    cd existing_folder
    git init
    git remote add origin https://gitlab.com/antoniogulino/TelegramBot.git
    git add .
    git commit -m "Initial commit"
    git push -u origin master

Existing Git repository
-----------------------

    cd existing_repo
    git remote add origin https://gitlab.com/antoniogulino/TelegramBot.git
    git push -u origin --all
    git push -u origin --tags
