Si tratta anzitutto di documentazione.
I file da modificare sono `CreareUnTelegramBot.md` e `InGiro.md`.

CreareUnTelegramBot.md
======================

I file `CreareUnTelegramBot.html` e   `CreareUnTelegramBot.pdf` vengono generati 
a partire dal file markdown `CreareUnTelegramBot.md`.

Il file markdown può essere trasformato in pdf o html (o word) usando p.es. `RStudio` o `pandoc`


In ambito linux, usando `pandoc`

    sudo apt-get install pandoc
    pandoc -s CreareUnTelegramBot.md -o Nome_a_scelta.pdf
    pandoc -s CreareUnTelegramBot.md -o Nome_a_scelta.html
    
InGiro.md
=========

Spiega le funzionalità e come si interagisce con il bot `InGiro`.