---
title: "Telegram Bot - una guida operativa"
author: "Antonio Gulino"
output:
  pdf_document: default
  html_document: default
  word_document: default
---




Attivare un Telegram-Bot
====================================
Chiamare `BotFather` e comparirà

> Was kann dieser Bot?
>
> BotFather is the one bot to rule them all. Use it to create new bot
> accounts and manage your existing bots.
>
> About Telegram bots:\
> `https://core.telegram.org/bots`
>
> Bot API manual:\
> `https://core.telegram.org/bots/api`
>
> Contact `@BotSupport` if you have questions about Bot API.

In basso si trova una sorta di bottone con la semplice scrita `Start`.
Premendolo (che equivale a scrivere `/start`) compare un testo
medio-lungo (che si ottiene pure scrivendo `/help`).


Dopo aver scritto al BotFather `/newbot` (basta anche soltanto cliccare
dentro la paginetta di help che è comparsa) Lui risponderà

> Alright, a new bot. How are we going to call it? Please choose a name
> for your bot.

Scegliere un nome per il proprio bot. Per esempio `IlMio` e Lui
risponderà

> Good. Now let’s choose a username for your bot. It must end in ‘bot‘.
> Like this, for example: TetrisBot or tetris\_bot.



Dunque bisogna scegliere uno *username* per il proprio bot. Per esempio
`IlMioBot`




A questo punto Lui risponde con



>  Done! Congratulations on your new bot. You will find it at
>  `t.me/IlMioBot`. You can now add a description, about section and
>  profile picture for your bot, see `/help` for a list of commands. By
>  the way, when you’ve finished creating your cool bot, ping our Bot
>  Support if you want a better username for it. Just make sure the bot
>  is fully operational before you do this.
> 
>  Use this token to access the HTTP API:
>  `235397970:CEXAA_3Xq0eac-MZzIbYfvYhXTGIlFjn`
>
>  For a description of the Bot API, see this page:
>  `https://core.telegram.org/bots/api`




Bene, adesso siamo in possesso di un *bot*.



Configurare il proprio bot 
===========================



Con il comando `/setdescription` ci viene proposto l’elenco dei nostri
bot (potremmo averne più di uno).


> Choose a bot to change description.

E noi scegliamo quello desiderato: `@IlMioBot`.


> OK. Send me the new description for the bot. People will see this
> description whe they open a chat with your bot, in a block titled
> ’What can this bot do?’.




Noi scriviamo quello che ci viene in mente. Per esempio: Quando sei in
giro puoi mandarmi coordinate, foto, audio, video e qualsiasi altro file
e ci penserò io a gestirlo.

> Success! Description updated. `/help`


Altri comandi utili in questa fase sono:


/setabouttext
: è una breve descrizione di se stesso che compare quando si clicca per sapere di più sulla “persona”.

/setuserpic
: gli si può assegnare una foto. Pare che con la mia versione su Ubuntu non si riesca
a mandare una foto, perché non vuole un file (anche se è una foto) ma
vuole un “invio foto”.

/setcommands
: si possono "inventare" dei comandi aggiungendo loro una breve descrizione

/setjoingroups
: si può decidere
se questo boto potrà far parte di un gruppo. Se disattivato non potrà
essere aggiunto a gruppi.

/setprivacy
: si si attiva,
allora il bot riceverà solo messaggi che cominciano con “`/`” oppure che
menzionano il bot usando il suo nome. Se invece si disattiva, allora
riceverà tutti i messaggi che i diversi partecipanti al gruppo mandano
al gruppo.


Comunicare in modo manuale con il bot: getUpdates 
=================================================


In generale si può interagire con il proprio bot tramite chiamate web
che contengono il *token* del proprio bot e un *metodo*, p.es. `getMe` (da
le basi per identificare il bot)

```{.Lyx-Code}
https://api.telegram.org/bot235397970:CEXAA_3Xq0eac-MZzIbYfvYhXTGIlFjn/getMe
```

che restituisce un oggetto *JSON*

    {"ok":true, "result":{"id":927073539, "first_name":"IlMio", "username":"IlMioBot"}}

che in pratica descrive questa struttura:


    ok = true
    result =
           id = 927073539
           first_name = "IlMio"
           username = "IlMioBot"



Effettivamente tutte le chiamate restituiscono un oggetto *JSON*.



Tra i diversi metodi ci sono anche

getUpdates
: elenca i messagi che ci sono a disposizione

sendMessage
: invia un messaggio.
I parametri obbligatori sono `chat_id` e `text`. Dunque la parte con il
metodo diventerebbe `sendMessage?chat_id=5326120&text=Ciao`

sendPhoto
: invia una foto. I parametri obbligatori sono `chat_id` e `photo`. Se la foto si trova già
sul server Telegram allora si può indicare il suo codice indentificativo
e non ci sono limiti sulla dimensione del file. Se la foto si trova in
internet, allora si indica la *url* e la foto non può essere più di
5 MB. P.es.
`sendPhoto?chat_id=5326120&photo=https://www.miosito.com/foto.jpg` .
È possibile anche mandare un file presente sul proprio PC, ma bisogna
usare un metodo avanzato (multipart/form-data) (vedasi:
`https://core.telegram.org/bots/api#sending-files`)

sendAudio
: invia un file `.mp3`.
I parametri obbligatori sono `chat_id` e `audio`. Se il file si trova
già sul server Telegram allora si può indicare il suo codice
indentificativo. Se il file si trova in internet, allora si indica la
*url* e il limite è di 20 MB. Il file audio verrà proposto dal music
player interno di Telegram.


sendVoice
: invia un file `.ogg`.
codificati OPUS. Si distingue da sendAudio anche per come viene gestito
dal Telegram del destinatario. I parametri obbligatori sono `chat_id` e
`voice`. Se il file si trova già sul server Telegram allora si può
indicare il suo codice indentificativo. Se il file si trova in internet,
allora si indica la *url.* Per tutte le varianti il limite è di 50 MB.

sendVideo
: invia un file `.mp4`.
I parametri obbligatori sono `chat_id` e `video`. Se la foto si trova
già sul server Telegram allora si può indicare il suo codice
indentificativo. Se il file si trova in internet, allora si indica la
*url* e il limite è di 20 MB.

sendVideoNote
: invia un file
`.mp4` di durata massima di 1 minuto, che verrà mostrato in forma
quadrata (rounded square). I parametri obbligatori sono `chat_id` e
`video_note`. Se la foto si trova già sul server Telegram allora si può
indicare il suo codice indentificativo. Se il file si trova in internet,
allora si indica la *url.*

sendDocument
: con questo metodo
si possono mandare qualsiasi tipo di file. Se si trova in internet,
allora sono ammessi solo i tipi `gif`, `pdf` e `zip`.

sendLocation
: invia coordinate
geografiche. I campi obbligatori sono `chat_id`, `latitude` e
`longitude`. Chi riceve il messaggio vede la solita goccia su una
cartina di `maps.google.com`. Esempio: 
`sendLocation?chat_id=5326120&latitude=46.4711&longitude=11.2624`

sendVenue
: come sendLocation ma
in più ci sono i campi obbligatori `title` e `address` e il campo
facoltativo `foursquare_id`. Esempio:\
`sendVenue?chat_id=5326120&latitude=46.4711&longitude=11.2624&title=MPreis&address=Eppan` .
Se non si usa il campo foursquare_id, allora *sendVenue* si distingue
da *sendLocation* solo per il fatto che sopra la cartina compaiono
queste due informazioni (title e address) aggiuntive. Con
l’identificativo di foursquare sul browser si apre la pagina del negozio
sul sito foursquare. Esempio:\
`sendVenue?chat_id=5326120&latitude=46.4711&longitude=11.2624&title=MPreis&address=Eppan&foursquare_id=50c3162ee4b0f9a0d77a3e02`

sendContact
: oltre al solito
`chat_id`, ci sono i campi obbligatori `phone_number` e `first_name`,
mentre `last_name` è facoltativo. Arriva un messaggio adeguatamente
formattato.




Per tutti i metodi `sendQualcosa` ci sono dei campi opzionali:

disable_notification=*true*
: fa in modo che chi riceve il messaggio non senta il beep.

reply_to_message_id
: se il messaggio dev’essere considerato la risposta ad uno ricevuto, allora
indicare il codice identificativo di tale messaggio.



Esistono poi tanti altri metodi riguardanti i gruppi o gli sticker.



Un metodo serve a cancellare messaggi inviati dal bot stesso:

deleteMessage
: ha solo due parametri, entrambi obbligatori: `chat_id` e `message_id`. Cosí si
possono cancellare solo quelli che ha inviato ma non quelli che ha
ricevuto.



Maggiore informazioni sui metodi disponibili e le loro opzioni:\
`https://core.telegram.org/bots/api#available-methods`


Uno script in PHP per automatizzare le azioni 
==================================================

vedasi: https://www.youtube.com/watch?v=hJBYojK7DO4



Creare il file test.php che metteremo successivamente sul nostro server (il quale DEVE essere
raggiungibile con https). 
Anzitutto nel `file.php` scriviamo le informazioni di base

```{.LyX-Code}
<?php
    $botToken = "235397970:CEXAA_3Xq0eac-MZzIbYfvYhXTGIlFjn";
    $website = "https://api.telegram.org/bot".$botToken;
?>
```

adesso facciamo in modo che riesca a vedere i messaggi che ci sono

```{.LyX-Code}
<?php
    $botToken = "235397970:CEXAA_3Xq0eac-MZzIbYfvYhXTGIlFjn";
    $website = "https://api.telegram.org/bot".$botToken;
        
    $update = file_get_contents($website."/getUpdates");
        
    print_r($update);
?>
```

eseguendo lo script da riga di comando

``` {.LyX-Code}
php test.php
```

oppure richiamando questo file dal nostro sito (che al momento può essere
ancora senza la "s" di https)

``` {.LyX-Code}
http://www.miosito.com/nomeLungoAssurdo/test.php
```


otteniamo il file JSON, che elenca i messaggi *ricevuti* dal bot (non quelli da lui inviati)


```
{"ok":true,
"result":[
    {"update_id":623411616, 
     "message":{"message_id":1,
                "from":{"id":5326120,
                        "first_name":"Mario",
                        "last_name":"Rossi",
                        "username":"SuperMan"
                        },
                "chat":{"id":5326120, 
                        "first_name":"Mario",
                        "last_name":"Rossi", 
                        "username":"SuperMan", 
                        "type":"private"
                        }, 
                "date":1503172026, 
                "text":"/start",
                "entities":[{"type":"bot_command", 
                             "offset":0,
                             "length":6
                             }]
                }
     }, 
    {"update_id":623411617, 
     "message":{"message_id":2, 
                "from":{"id":5326120, 
                        "first_name":"Mario",
                        "last_name":"Rossi",
                        "username":"SuperMan",
                        "language_code":"de-DE"
                        }, 
                "chat":{"id":5326120, 
                        "first_name":"Mario",  
                        "last_name":"Rossi",  
                        "username":"SuperMan",
                        "type":"private"
                        },
                "date":1503172032,
                "text":"Ciao!"
                }
     },
    {"update_id":623411618, 
     "message":{"message_id":4, 
                "from":{"id":5326120, 
                        "first_name":"Mario", 
                        "last_name":"Rossi", 
                        "username":"SuperMan", 
                        "language_code":"de-DE"
                        },
                "chat":{"id":5326120, 
                        "first_name":"Mario",
                        "last_name":"Rossi",
                        "username":"SuperMan", 
                        "type":"private"
                        }, 
                "date":1503172330, 
                "text":"non capisco..."
                }
    }]
}
```



bene, cominciamo a gestire la lista

```{.LyX-Code}
<?php
    $botToken = "235397970:CEXAA_3Xq0eac-MZzIbYfvYhXTGIlFjn";
    $website = "https://api.telegram.org/bot".$botToken;
        
    $update = file_get_contents($website."/getUpdates");
    $updateArray = json_decode($update, TRUE);

    print_r($updateArray);
?>
```
Adesso la risposta dello script non è più un file JSON, ma un array in
formato php.

```
Array
(
    [ok] => 1
    [result] => Array
        (
            [0] => Array
                (
                    [update_id] => 623411616
                    [message] => Array
                        (
                            [message_id] => 1
                            [from] => Array
                                (
                                    [id] => 5326120
                                    [first_name] => Mario
                                    [last_name] => Rossi
                                    [username] => SuperMan
                                )

                            [chat] => Array
                                (
                                    [id] => 5326120
                                    [first_name] => Mario
                                    [last_name] => Rossi
                                    [username] => SuperMan
                                    [type] => private
                                )

                            [date] => 1503172026
                            [text] => /start
                            [entities] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => bot_command
                                            [offset] => 0
                                            [length] => 6
                                        )

                                )

                        )

                )

            [1] => Array
        ecc., ecc.

```
 
    
che ci permette di accedere ai vari elementi

```{.LyX-Code}
<?php
    $botToken = "235397970:CEXAA_3Xq0eac-MZzIbYfvYhXTGIlFjn"; 
    $website = "https://api.telegram.org/bot".$botToken;

    $update = file_get_contents($website."/getUpdates");
    $updateArray = json_decode($update, TRUE);

    $text = $updateArray["result"][0]["message"]["text"];
    $chatId = $updateArray["result"][0]["message"]["chat"]["id"];
    
    print_r("text = ".$text."\n");
    print_r("chat_id = ".$chatId."\n");
?>
```
il risultato diventa

    text = /start
    chat_id = 5326120


Adesso abbiamo i rudimenti per reagire inviando "test_con_php" alla
chat identificata con chatId:

```{.LyX-Code}
<?php
    $botToken = "235397970:CEXAA_3Xq0eac-MZzIbYfvYhXTGIlFjn"; 
    $website = "https://api.telegram.org/bot".$botToken;

    $update = file_get_contents($website."/getUpdates");
    $updateArray = json_decode($update, TRUE);

    $text = $updateArray["result"][0]["message"]["text"];
    $chatId = $updateArray["result"][0]["message"]["chat"]["id"];
    
    print_r("text = ".$text."\n");
    print_r("chat_id = ".$chatId."\n");

    file_get_contents($website."/sendMessage?chat_id=".$chatId."&text=test_from_php");
?>
```
La richiesta via web

``` {.LyX-Code}
https://api.telegram.org/bot235397970:CEXAA_3Xq0eac-MZzIbYfvYhXTGIlFjn   
/sendMessage?chat_id=5326120&text=test_from_php
```    

invia il messaggio "`test_con_php`" all'utente `5326120` e il server risponde con

    {"ok":true,
     "result":
         {"message_id":28,
          "from":{"id":927073539,
                  "first_name":"IlMio","username":"IlMioBot"
                 },
          "chat":{"id":5326120,
                  "first_name":"Mario","last_name":"Rossi","username":"SuperMan",
                  "type":"private"
                  },
          "date":1503226690,
          "text":"test_from_php"
          }
    }
    
che in realtà è contenuto in un'unica, lunga riga, ma per motivi di leggibilità qui è stata suddivisa in più righe.



setWebhook - Il proprio sito gestisce il bot: 
====================================================================


vedasi nella parte finale del filmato: https://www.youtube.com/watch?v=hJBYojK7DO4

Anzitutto i requisiti di base

* avere un proprio sito, raggiungibile con `https` (p.es. `https://miosito.com`)
* il sito deve poter eseguire *php*


Adesso passiamo all’automazione più avanzata, dove è bene sapere che
finquando è  attivato *webhook* non è possibile richiamare
manualmente `/getUpdates`. Però per fortuna nulla è definitivo: con
`/deleteWebhook` si può disatttivare il webhook per tornare su
"manuale". Dice la documentazione: 

> Use this method to remove webhook integration 
> if you decide to switch back to getUpdates. 
> Returns True on success. 
> Requires no parameters.


Per motivi di sicurezza creiamo una directory
con un nome lungo e assurdo (che non diremo a nessuno in giro!). Questo ci
permette di essere sicuri (si fa per dire) che il nostro script venga
chiamato solo dai server di Telegram.
Per esempio `nomeLungoAssurdo` oppure `zIdnEiOGzjTddcOEaxcswstIZuWlam5sWue25SaD`, ottenuto usando R (https://www.r-project.org),

```{.LyX-Code}
> caratteri <- c(LETTERS, letters, paste(0:9))
> k <- NROW(caratteri)
> n <- 40
> paste(caratteri[ceiling(runif(n,1,k))],collapse="")
[1] "zIdnEiOGzjTddcOEaxcswstIZuWlam5sWue25SaD"
```

Comunque, ecco come si
comunica a Telegram chi farà il lavoro di *bot*:

``` {.LyX-Code}
https://api.telegram.org/bot235397970:CEXAA_3Xq0eac-MZzIbYfvYhXTGIlFjn
/setWebhook?url=https://www.miosito.com/nomeLungoAssurdo/test.php
```

Se tutto è corretto, dovremmo ottenere la risposta JSON

    {"ok":true,"result":true,"description":"Webhook was set"}




Adesso modifichiamo il file `test.php`, perché la presenza di nuovi
messaggi ci viene segnalata direttamente dai server Telegram i quali
adesso sanno chi devono interpellare. Cosicché nella funzione
`file_get_contents()` si sostituisce `$website."/getUpdates"` con
`"php://input"`. Adesso non arriva però più un elenco di messaggi, 
ma sempre solo uno alla volta percui lo script diventa


``` {.LyX-Code}
<?php
    $botToken = "235397970:CEXAA_3Xq0eac-MZzIbYfvYhXTGIlFjn"; 
    $website = "https://api.telegram.org/bot".$botToken;

  
    $update = file_get_contents("php://input");
    $updateDecode = json_decode($update, TRUE);

    $text = $updateDecode["message"]["text"];
    $chatid = $updateDecode["message"]["chat"]["id"];

    file_get_contents($website."/sendMessage?chat_id=".$chatid."&text=echo:".$text);
?>
```

che ha come conseguenza che ogni volta che qualcuno scrive al bot un
messaggio lui risponde con lo stesso messaggio. Sta a
noi renderlo più intelligente cambiano il testo delle risposta o anche il destinatario.

Sapendo che il *bot* riceve un oggetto *JSON* del tipo

    {"update_id":623411643,
     "message":{"message_id":70,
                "from":{"id":5326120,
                        "first_name":"Mario",
                        "last_name":"Rossi",
                        "username":"SuperMan",
                        "language_code":"de-DE"
                        },
                "chat":{"id":5326120,
                        "first_name":"Mario",
                        "last_name":"Rossi",
                        "username":"SuperMan",
                        "type":"private"
                        },
                "date":1503231285,
                "text":"testo"
                }
    }

potremmo rendere il nostro *bot* meno stupido usando le altre informazioni ricevute:
 
    $nome    = $updateDecode["message"]["from"]["first_name"];
    $cognome = $updateDecode["message"]["from"]["first_name"];
    $lingua  = $updateDecode["message"]["from"]["language_code"];
    
e potremmo cosí renderlo più personale. La parte con `decode` permette di sapere che cosa
c'è nell'oggetto *JSON* che riceve il *bot*.

``` {.LyX-Code}
<?php
    $botToken = "235397970:CEXAA_3Xq0eac-MZzIbYfvYhXTGIlFjn"; 
    $website = "https://api.telegram.org/bot".$botToken;

  
    $update = file_get_contents("php://input");
    $updateDecode = json_decode($update, TRUE);

    $text = $updateDecode["message"]["text"];
    $chat_id = $updateDecode["message"]["chat"]["id"];
    $nome    = $updateDecode["message"]["from"]["first_name"];
    $cognome = $updateDecode["message"]["from"]["first_name"];
    $lingua  = $updateDecode["message"]["from"]["language_code"];
    $caption = $updateDecode["message"]["caption"];
    
    if ($lingua == "de-DE") {$messaggio = 'Guten Tag '. $nome . "! ";}
    elseif ($lingua == "it-IT") {$messaggio = 'Buon giorno '. $nome . "! ";}
    else {$messaggio = $nome . ', qe tsa trena to ' . $lingua . '?';};
    
    
    if ($chat_id == 5326120) {
        if ($lingua == "de-DE") {$messaggio = $messaggio . 'Was meinst du mit "';}
        elseif ($lingua == "it-IT") {$messaggio = $messaggio . 'Cosa intendevi dire con "';}
        else {$messaggio = $messaggio . 'Je kro taar ne "';};
        $messaggio = $messaggio . $text . '"?';
    } else {
        if ($lingua == "de-DE") {$messaggio = $messaggio . 'Kennen wir uns?';}
        elseif ($lingua == "it-IT") {$messaggio = $messaggio . 'Ci conosciamo?';};
    }

    if (substr($text,0,7) == '/decode') {
        $messaggio = $messaggio . " ::JSON::".$update;
    } elseif (substr($caption,0,7) == '/decode') {
        $messaggio = $messaggio . " ::JSON::".$update;
    } elseif ($updateDecode["message"]["entities"][0]["type"] == "bot_command") {
        $messaggio = $messaggio . " ::JSON::".$update;
    } elseif ($text == '') {
        $messaggio = $messaggio . " ::JSON::".$update;
    }

    file_get_contents($website."/sendMessage?chat_id=".$chat_id."&text=".$messaggio);
?>
```


L'oggetto *JSON* ricevuto dal *bot*
===========================================================

Struttura di base
-----------------

```
"update_id"
"message"
    "message_id" 
    "from"       
    "chat"       
    "date"       
    "text"       <solo messaggi di testo, evtl. di risposta in una sessione "live" di coordinate>
    "entities"   <solo messaggi di testo contenenti comandi o url>
    "document"   <anche invio di immagini>
    "photo"      <solo invio di immagini>
    "caption"    <solo invio di immagini contenenti commento>
    "audio"      <solo invio mp3 e ogg>
    "voice"      <messaggio vocale spedito da dentro Telegram. Formato ogg>
    "video"      <solo invio mov>
    "reply_to_message" <in risposta in una sessione "live" di coordinate>
    "location"   <inviando una coordinata facendo uso della funzione interna di Telegram>
```

In tutti i messaggi
-----------------------------------------

```
"update_id"
"message"
    "message_id"
    "from"
        "id"
        "first_name"
        "last_name"
        "username"
        "language_code" = <p.es. "de" o "de-DE">
    "chat"
        "id"
        "first_name"
        "last_name"
        "username"
        "type"      = <"private" nel caso di un messaggio non tramite gruppo>
    "date" = <espressi in secondi dall'inizio dell'era informatica>
    ....
             
```


Nei messaggi di testo
---------------------------------
L'oggetto ```message :: text``` viene inviato sia quando si mandono semplicemente messaggi,
ma anche quando si manda un messaggio in "risposta" dentro una sessione "live" di coordinate.

```
"update_id"
"message"
    ....
    "text" = <il contenuto del testo inviato>
```

Nell'invio di immagini
---------------------------------

### Immagini *JPEG*  o *PNG* con compressione
Ci sono le informazioni sia sull'immagine originale che su alcune varianti di formato ridotto.

```
"update_id"
"message"
    ....
    "caption" = <il contenuto del commento allegato alla foto>
    "photo" 
        0
            "file_id" = <una lunga stringa assurda>
            "file_size"
            "width"
            "height"
            "file_path" = "photos/<una sequenza di numeri>.jpg" <solo per jpg>
        1
            "file_id" = <una lunga stringa assurda>
            "file_size"
            "width"
            "height"
        2
            "file_id" = <una lunga stringa assurda>
            "file_size"
            "width"
            "height"
```

### Immagini *PNG* e *JPG* senza compressione
Quando non si attiva il *flag* "compressione", allora le immagini vengono 
trasmesse non in quanto `photo` come come `document`. Rispetto allo standard, 
ci sono pure informazioni sul `thumb` tipiche delle foto.

```
"update_id"
"message"
    ....
    "caption" = <il contenuto del commento allegato alla foto>
    "document" 
        "file_name" = <cosí come è stato inviato>
        "mime_type" = "image/png"
        "file_id"   = <una lunga stringa assurda>
        "file_size" = <Byte>
        "thumb"
            "file_id" = <una lunga stringa assurda>
            "file_size"
            "width"
            "height"
    ....
             
```



Nell'invio di video (mov,..)
---------------------------------
Non si viene a sapere il nome del file.

```
"update_id"
"message"
    ....
    "video"
        "mime_type"= <"video/quicktime" (.mov)>
        "duration" = <secondi>
        "width"
        "height"
        "file_id"  = <una lunga stringa assurda>
        "file_size"= <Byte>
        "thumb"
            "file_id" = <una lunga stringa assurda>
            "file_size"
            "width"
            "height"
```



Nell'invio di audio (mp3, ogg)
---------------------------------
A seconda che si invii un file oppure si effettui una registrazione
si ottiene l'oggetto `audio` nel primo caso e `voice` nel secondo.
Nel primo caso il sistema estrae dal file `.mp3` l'informazione sul titolo e sull'autore del brano.

### Spedendo un file

Non si viene a sapere il nome del file.

```
"update_id"
"message"
    ....
    "audio"
        "duration" = <secondi>
        "mime_type"= <"audio/mpeg" oppure "audio/x-vorbis ogg">
        "title"    = <informazione estratta dalle info nel file>
        "performer"= <informazione estratta dalle info nel file>
        "file_id"  = <una lunga stringa assurda>
        "file_size"= <Byte>
    
```

### Mandando una registrazione vocale da dentro Telegram

```
"update_id"
"message"
    ....
    "voice"
        "duration" = <secondi>
        "mime_type"= "audio/ogg"
        "file_id"  = <una lunga stringa assurda>
        "file_size"= <Byte>
        
```


Nell'invio di file qualsiasi (txt, pdf, gpx, ...)
---------------------------------

```
"update_id"
"message"
    ....
    "document"
        "file_name" = <il nome originale del file>
        "mime_type" = 
        "file_size" = <Byte>
        "file_id"   = <una lunga stringa assurda>
        "thumb"     <solo se si tratta di immagini mime_type="image/...">
    
```
Finora ho identificato alcune varianti di `mime_type`: 

File di testo semplici

```
    "text/plain" (.txt) 
    "text/vcard" (.vcf)
    "text/x-c  src" (.cpp)
    "text/x-python" (.py)
    "text/x-r-source" (.R)
    "application/x-php" (.php)
    "text/calendar" (.ics)
    "text/html" (.html)
    "text/tab-separated-values" (.tsv)
    "text/csv" (.csv)
    "application/xml" (.xml)
    "application/gpx xml" (.gpx)
    "application/json" (.json)
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document" (.docx)
```

formati da aprire con appositi software   

```
    "image/png" (.png)
    "application/pdf" (.pdf) 
    "application/zip" (.zip)
    "application/x-executable"
```




Nell'invio di coordinate geografiche
---------------------------------
Coordinate geografiche in quanto tali arrivano in più modi:

* inviando un singolo punto
* avviando una sessione "live" (in questo caso viene mandato unicamente il punto nel quale ci si trovava mentre si avviava la sessione)
* rispondendo con messaggi, foto, ecc. all'interno di una sessione "live"


### inviando un singolo punto o avviando la sessione "live"
Dalla risposta JSON non si riesce a distinguere se è stato spedito un singolo punto oppure se è stata avviata una sessione live.
Nel caso si tratti di una sessione "live", allora il suo `message_id` verrà usato in seguito per sapere durante quale sessione 
viene mandato ulteriore materiale (georeferenziato).

```
"update_id"
"message"
    ....
    "date"
    "location"
        "latitude"  = <p.es. 46.7890123>
        "longitude" = <p.es. 11.9876543>
```

### Rispondendo con qualcosa durante una sessione live
Le coordinate sono dentro l'"oggetto" "reply_to_message", mentre il "aulcosa" (testo, audiomessaggio, ecc.) è al "primo livello" di "message". 

Gli oggetti "from" e "chat" presenti nell'oggetto "reply_to_message" pare che siano uguali a quelli soliti  al "primo livello" di "messate".

Ci sono più date (oltre a quella che misura il server stesso):

* ```message :: date```
* ```message :: reply_to_message :: date```
* ```message :: reply_to_message :: edit_date```

È possibile sapere tramite `reply_to_message :: message_id` a quale sessione "live" fa riferimento.


```
"update_id"
"message"
    ....
    "date"
	"text":"Risposta"
	"reply_to_message"
        "message_id" = <p.es. 864, fa riferimento all'ID del messaggio che ha avviato la sessione live>
		"date"       = <al momento dell'avvio della sessione live>
		"edit_date"  = <al momento in cui si è cominciato a scrivere la risposta>
		"location"
		    "latitude"  = <p.es. 46.7890123>
			"longitude" = <p.es. 11.9876543>
		"from"
		    "id" = <p.es. 532140612, uguale al "id" di "chat">	
			"is_bot":false
			"first_name" = <nome di chi ha spedito il messaggio>
			"last_name"  = <cognome di chi ha spedito il messaggio>
			"username"   = <nomeIDtelegram di chi ha spedito il messaggio, opzionale>
			"language_code" = <p.es. "it", "de">
	    "chat"
	        "id" = <p.es. 532140612, uguale al "id" di "from">	
				"first_name" = <nome di chi ha spedito il messaggio>
				"last_name"  = <cognome di chi ha spedito il messaggio>
				"username"   = <nomeIDtelegram di chi ha spedito il messaggio, opzionale>
				"type"       = <p.es. "private">

```


In presenza di comandi
----------------------------------
Nei diversi messaggi ci potrebbero essere nel testo (``["message"]["text"]``) dei comandi riconosciuti come tali 
dal *bot* (p.es. `/json`, `/ls`, `/decode`, `/beep`, `/pippo`,...)

In tal caso si trova un array (un elemento per ciascun comando presente) 
che indica in quale posizione si trova e la lunghezza (ma non dice che comando sia).

```
   "update_id"
   "message"
       ....
       "entities"
           0
               "type"  = "bot_command"
               "offset"= <la posizione iniziale nel testo>
               "length"=
           1 
               "type"  = "bot_command"
               "offset"= 
               "length"=
           ....       
```

Ad esempio, venuti a sapere che il primo elemento è un comando,
allora che tipo di comando si tratti può essere scoperto con

    $posizione = $updateDecode["message"]["entities"][0]["offset"];
    $lunghezza = $updateDecode["message"]["entities"][0]["length"];
    $comando = substr($updateDecode["message"]["text"],$posizione,$lunghezza);
    
In presenza di link    
-------------------
Nel testo del messaggio ricevuto dal *bot* potrebbero esserci dei link.

In tal caso si trova un array (un elemento per ciascun comando presente) 
che indica in quale posizione si trova e la lunghezza (ma non dice che comando sia).

```
   "update_id"
   "message"
       ....
       "entities"
           0
               "type"  = "url"
               "offset"= <la posizione iniziale nel testo>
               "length"=
           1 
               "type"  = "url"
               "offset"= 
               "length"=
           ....       
```


Salvare i file ricevuti con il bot
==================================

Aggiungere p.es. il seguente codice

```
    if ($updateDecode["message"]["document"]["mime_type"]=="image/jpeg") {
    
        $file_name = $updateDecode["message"]["document"]["file_name"];
        $file_id = $updateDecode["message"]["document"]["file_id"];
        
        $thumb_file_id = $updateDecode["message"]["document"]["thumb"]["file_id"];
        
        $urlRichiestaFilepath =  $website."/getFile?file_id=".$file_id;
        $RispostaFilepath = file_get_contents($website."/getFile?file_id=".$file_id);
        $decodeRispostaFilepath = json_decode($RispostaFilepath, TRUE);
        
        $file_path = $decodeRispostaFilepath["result"]["file_path"];
       
        $urlRichiestaFile = "https://api.telegram.org/file/bot".$botToken."/".$file_path;
        
        $file_path_salvare = "downloads/".$file_name;
        $rispostaDownloadFile = file_put_contents(    $file_path_salvare,     file_get_contents( $urlRichiestaFile ) );
        
        $messaggio = 'Salvato ' . $file_path_salvare . ' ('. $rispostaDownloadFile . 'Bytes). Vedasi http://www.miosito.com/nomeLungoAssurdo/'.$file_path_salvare;
        
        file_get_contents($website."/sendMessage?chat_id=".$chat_id."&text=".$messaggio);
    }
```